import { Router } from 'express';
let router = Router();
import {UsersController} from '../../controllers/users-controller/users-controller';
//import RestriccionesController from '../controllers/restricciones.controller';

router.route('/')
	.get(UsersController.index)
    .post(UsersController.newUser);

module.exports = router ;