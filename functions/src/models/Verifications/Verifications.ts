import {Schema, model} from 'mongoose';

const VerificationSchema = new Schema({
    rut:{
        type: String,
        required: true
    },
    code: {
    type: String,
    required: true,
        default: function () {
            return (Math.floor(Math.random() * 10000) + 10000).toString().substring(1);
        }
    },
    dateCreated: {
        type: Date,
        default: Date.now()
    }
    },{
        timestamps: true,
        collection: "Verification"
});

export default model('Verification',VerificationSchema);