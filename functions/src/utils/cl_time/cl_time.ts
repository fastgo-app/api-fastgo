import * as moment from 'moment-timezone'
//excel solicitudes
export class chileanTime {
    static getHora(fecha: string) {
        if(!fecha) return ""
        let now = moment(fecha).tz('America/Santiago');
        let date = now.add(now.utcOffset(), 'minutes').toDate();
        return date; 
    }
}

export default chileanTime