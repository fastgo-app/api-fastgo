import transporter from "../mailer/mailer";
import * as fs from "fs";
import * as handlebars from "handlebars";
import { promisify } from "util";
import { chileanTime } from "../../utils/cl_time/cl_time";

const readFile = promisify(fs.readFile);

handlebars.registerHelper("link", function(restext, resurl) {
    let url = handlebars.escapeExpression(resurl),
        text = handlebars.escapeExpression(restext)
        
   return new handlebars.SafeString("<a href='" + url + "'>" + text +"</a>");
});

export class Emailer {
    static async sendEmail(email_sending: any, params: any, subject_data: any, con_copia=true) {
        let html, replacements: any
    
        html = await readFile("./src/mailers/default_email.html", "utf8");
        replacements = {
            title: params.title,
            subtitle: params.subtitle,
        };

        let htmltemplate = handlebars.compile(html);
      
        var htmlToSend = htmltemplate(replacements);
        let sending = null
        try {
          sending = await transporter.sendMail({
            from: "no-reply@fastgo.com",
            to: email_sending,
            subject: subject_data,
            html: htmlToSend,
            bcc: con_copia ? 'validacion@fastgo.com': []
          });
          console.log("sending", sending)
          return sending;
        } catch (error) {
          console.log("error ending email:", error)
          return sending;
        }
    }
}
export default Emailer;
  