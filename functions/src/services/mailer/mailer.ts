import * as dotenv from 'dotenv';
dotenv.config({path: __dirname + '/../../.env'});

const nodemailer = require("nodemailer");
console.log("Mailer User", process.env.FASTGO_USER)
console.log("Mailer PASSWORD", process.env.FASTGO_PW)
let transporter = nodemailer.createTransport({
  host: process.env.MAILER_HOST,
  port: 465,
  secure: true, // true for 465, false for other ports
  pool: true,
  maxConnections: 5,
  maxMessages: 5,
  auth: {
    user: process.env.MAILER_USER, 
    pass: process.env.MAILER_PASSWORD
  },
});

export default transporter