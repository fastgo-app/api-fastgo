import * as functions from "firebase-functions";
import * as express from 'express';

let app = express();

app.get('/hello-world2', (req,res) => {
    return res.status(200).json({message: 'Hello world'});
});

exports.app = functions.https.onRequest(app);

// // Start writing Firebase Functions
// // https://firebase.google.com/docs/functions/typescript
//
// export const helloWorld = functions.https.onRequest((request, response) => {
//   functions.logger.info("Hello logs!", {structuredData: true});
//   response.send("Hello from Firebase!");
// });
